# Specify the image from which you are working
FROM atlas/analysisbase:21.2.51
# Copy the repo root into / code
COPY . /code
# Permanently change workdirectory
WORKDIR /code
RUN source ~/release_setup.sh && \
    sudo chown -R atlas /code && \
    g++ -o helloworld helloworld.cxx